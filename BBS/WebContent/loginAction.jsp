<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "user.UserDAO" %> <%--우리가 만든 클래스를 불러온다--%>
<%@ page import = "java.io.PrintWriter" %> <%--자바 스크립트 문장을 사용하기 위해 씀--%>
<% request.setCharacterEncoding("UTF-8"); %> <%--건너오는 모든 데이터를 변환--%>
<jsp:useBean id = "user" class = "user.User" scope = "page" /> <%--javabean를 사용한다--%>
<jsp:setProperty property="userID" name="user"/>
<jsp:setProperty property="userPassword" name="user"/> <%--아이디와 패스워드를 받아옴--%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv = "Content-Type" content="text/html; charset=UTF-8">
<title>JSP 게시판 웹 사이트</title>
</head>
<body>
	<%
		String userID = null;
		if(session.getAttribute("userID") != null) {
			userID = (String) session.getAttribute("userID");
		}
		if (userID != null) {
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('이미 로그인이 되어있습니다.')");
			script.println("location.href = 'main.jsp'");
			script.println("</script>");
		}
		UserDAO userDAO = new UserDAO(); //인스턴스 만듬
		int result = userDAO.login(user.getUserID(), user.getUserPassword()); //로그인을 시도할 수 있게 함 (입력값이  result에 담김)
		if ( result == 1) {
			session.setAttribute("userID", user.getUserID());
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("location.href = 'main.jsp'"); //로그인에 성공했을 때
			script.println("</script>");
		}
		else if (result == 0) {
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('비밀번호가 틀립니다.')");
			script.println("history.back()"); //이전 페이지로 사용자를 돌려보냄
			script.println("</script>");
		}
		else if (result == -1) {
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('존재하지 않는 아이디입니다.')");
			script.println("history.back()");
			script.println("</script>");
		}
		else if (result == -2) {
			PrintWriter script = response.getWriter();
			script.println("<script>");
			script.println("alert('데이터베이스 오류가 발생했습니다.')");
			script.println("history.back()");
			script.println("</script>");
		}
	%>
</body>
</html>